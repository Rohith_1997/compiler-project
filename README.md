# README #

Compiler Project

### What is this repository for? ###

* This repository contains all the files involved in making of a compiler for subset of C language
* This is a part of our compiler design lab

### What does this repository contain? ###
* c.lex : this is the lexer file which converts C code into tokens
* c.grm : this is the parser file which converts tokens into abstract syntax tree
* ast.sml : contains the data structures of the abstract syntax tree
* parser.sml : this file combines the parser and lexer and takes the input c file and gives the ast data structure
* translate.sml : converts ast data structure into python code	
* driver.sml : this file takes c file as input and gives the python code
* sources.cm : make file for SML interactive mode
* ctopy.mlb : list of all files to be loaded during runtime
* Makefile : makefile for the compiler



### How do I get set up? ###

* Clone this repository 
* In the terminal enter command "make"
* then run the "ctopy" file generated as "./ctopy <input>.c"
* the converted python code will be stored in "out.py"
* to clean the repository enter "make clean"

### Who do I talk to? ###

* Gautam Kumar
* Rohith Reddy G
